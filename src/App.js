import { useEffect, useState } from "react";




function App() {
  const [input1, updateInput1] = useState(localStorage.getItem("input1memory"))
  const [input2, updateInput2] = useState(localStorage.getItem("input2memory"))
  const handelChange1 = (event)=>{
    updateInput1(event.target.value)
  } 
  const handelChange2 = (event)=>{
    updateInput2(event.target.value)
  } 

  useEffect(() => {
    localStorage.setItem("input1memory",input1)
    localStorage.setItem("input2memory",input2)
  });
  return (
    <div style={{margin:"0 200px",width:"20%", backgroundColor:"#001a33",textAlign:"center"}}>
      <input value={input1} onChange={handelChange1} style={{backgroundColor:"#ccffff", marginLeft:"20px", width:"200px"}} />
      <input value={input2} onChange={handelChange2} style={{backgroundColor:"#ccffff", marginLeft:"20px", width:"200px"}} />
      <div><p style={{color:"#ccffff",marginLeft:"20px"}} >{input1} {input2}</p></div>
    </div>
  );
}

export default App;
